class CreateBusinessData < ActiveRecord::Migration
  def change
    create_table :business_data do |t|
      t.string :business_id
      t.string :full_address
      t.string :schools
      t.boolean :open
      t.string :categories
      t.string :photo_url
      t.string :city
      t.integer :review_count
      t.string :name
      t.string :neighborhoods
      t.string :url
      t.string :state
      t.float :stars
      t.string :type

      t.timestamps
    end
  end
end
