class DropTypeFromBusinessData < ActiveRecord::Migration
  def up
    remove_column :business_data, :type
  end

  def down
  end
end
