class CreateReviewData < ActiveRecord::Migration
  def change
    create_table :review_data do |t|
      t.string :user_id
      t.string :review_id
      t.integer :stars
      t.datetime :date
      t.string :text
      t.string :type
      t.string :business_id

      t.timestamps
    end
  end
end
